package dharmangsoni.imageuploadapivolley;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import java.io.File;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int RESULT_LOAD_IMG = 1;
    private ImageView selectedImage;
    private String physicalFilePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        selectedImage = (ImageView) findViewById(R.id.selectedImage);
        findViewById(R.id.selectImage).setOnClickListener(this);
        findViewById(R.id.uploadToServer).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.selectImage:
                pickImageFromGallery();
                break;
            case R.id.uploadToServer:
                uploadToServer();
                break;
        }
    }

    // For get image from gallery
    private void pickImageFromGallery() {
        // Create intent to Open Image applications like Gallery, Google Photos
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        // Start the Intent
        startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == RESULT_LOAD_IMG && data != null) {
            Uri selectedImageURI = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            // Get the cursor
            Cursor cursor = getContentResolver().query(selectedImageURI,
                    filePathColumn, null, null, null);
            if (cursor != null) {
                // Move to first row
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);

                // Required physical path of file so getting _data
                physicalFilePath = cursor.getString(columnIndex);
                cursor.close();
                // Set the Image in ImageView after decoding the String
                selectedImage.setImageBitmap(BitmapFactory.decodeFile(physicalFilePath));
            }
        }
    }


    // Volley request to upload
    // Check DeviceUserImageUpload class for full volley code request i have made
    private void uploadToServer() {
        String url = "http://technople.in/careq/api/device/addimage/100/60/format/json";
        DeviceUserImageUpload upload = new DeviceUserImageUpload(url,
                new File(physicalFilePath), new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                String resultResponse = new String(response.data);
                Log.e(">>>", resultResponse + "<<<");
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(">>", error.getMessage(), error);
            }
        });
        upload.setTag("testing");
        Volley.newRequestQueue(this).add(upload);
    }

}
